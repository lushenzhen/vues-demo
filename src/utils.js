import qs from 'qs'

export default {
  isProdEnv() {
    if (window.location.pathname.includes('static')) {
      return false
    } else if (window.location.hostname.includes('local.')) {
      return 'dev' // 开发环境, php服务代理
    } else if (!window.location.hostname.includes('local')) {
      return 'pro'
    }
  },
  concatParams(url, params) {
    const query = qs.stringify(params);
    return url.indexOf('?') === -1 ? url + '?' + query : url + '&' + query
  }
}
