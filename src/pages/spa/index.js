import Vue from 'vue'
import App from './App'
import router from './router'
import http from './http'
import VueAxios from 'vue-axios'
import {Toast, Loading} from 'vue-ydui/dist/lib.rem/dialog';
import './ydui.flexible'
import 'vue-ydui/dist/ydui.rem.css';
import 'vue-ydui/dist/ydui.base.css';

var app = {
  /**
   * 加载Vue
   */
  loadVue() {

    /* 设置为 false 以阻止 vue 在启动时生成生产提示 */
    Vue.config.productionTip = false

    /* 用于开发环境 */
    Vue.config.devtools = true

    /* 开发中追踪组件初始化、编译、渲染和打补丁的性能 */
    Vue.config.performance = true

    /* 全局错误处理 */
    Vue.config.errorHandler = function(err, vm, info) {
      // handle error
      // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
      // TODO
      console.error(err)
    }
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      template: '<App/>',
      components: {App}
    })
  },
  /**
   * 注册插件
   */
  loadUse() {
    Vue.use(VueAxios, http)
  },
  /**
   * 注册全局组件
   */
  loadCustomComponent() {
    Vue.prototype.$yd = {
      toast: Toast,
      loading: Loading,
    };
  },
  /**
   * 初始化
   */
  init() {
    this.loadCustomComponent()
    this.loadUse()
    this.loadVue()
  },
}
app.init()
