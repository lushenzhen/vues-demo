import axios from 'axios'
import {Toast, Loading} from 'vue-ydui/dist/lib.rem/dialog';
// 超时时间
axios.defaults.timeout = 20000

// 请求拦截器
axios.interceptors.request.use(config => {
  if(config.method === 'get'){
    config.url += `${config.url.includes('?') ? '&': '?'}ajax=1`
  }
  Loading.open('很快加载好了')
  return config
}, error => {
  Loading.close()
  Toast({mes: '加载超时', icon: 'error',timeout: 6000})
  return Promise.reject(error)
})

// 响应拦截器
axios.interceptors.response.use(result => {
  Loading.close()
  if (result.status && result.data.code == 200) {
    return result.data
  }
}, error => {
  Loading.close()
  Toast({mes: '加载失败', icon: 'error',timeout: 10000})
  return Promise.reject(error)
})

export default axios
