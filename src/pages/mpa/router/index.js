/**
 * 路由 ,如需子路由在业务模块下建 router.js 在父级引入
 */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'Default',
    component: r => require.ensure([], () => r(require(`@m/components/Default`)), `components/default`)
  }
  ]
})
