import axios from 'axios'
/* import utils from './utils' */
import {
    Loading,
    Message
} from 'element-ui'

// 超时时间
axios.defaults.timeout = 20000

// 请求拦截器
let loadingInstace
axios.interceptors.request.use(config => {
  if(config.method === 'get'){
      config.url += `${config.url.includes('?') ? '&': '?'}ajax=1`
  }
    loadingInstace = Loading.service({
        fullscreen: true
    })
    return config
}, error => {
    loadingInstace.close()
    Message.error({
        message: '加载超时'
    })
    return Promise.reject(error)
})

// 响应拦截器
axios.interceptors.response.use(result => {
    loadingInstace.close()
    if (result.status && result.status == 200) {
        return result.data
    }else {
      return Promise.reject(result.data.code + ':' + result.data.msg)
    }
}, error => {
    loadingInstace.close()
    Message.error({
        message: '加载失败'
    })
    return Promise.reject(error)
})

export default axios
