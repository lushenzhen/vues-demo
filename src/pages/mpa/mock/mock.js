const Mock = require('mockjs');
Mock.mock(
  /^\/admin\/adminapi\/menu_list/, 'get', [
    {
      label: '控制面板', // 菜单名称
      url: '/admin/dashboard', // 菜单对应URL
      icon: '', // icon
      isHref: true
    },
    {
      label: '账户管理',
      url: '/admin/admin/admin_list', // 菜单对应URL
      icon: '', // icon
      isHref: true
    },
    {
      label: '学院管理',
      children: [ // 子节点
        {
          label: '学校管理',
          url: '/admin/school/school_list',
          isHref: true   // 是否是老系统URL
        },
        {
          label: '社团管理',
          url: '/admin/Club/club_list',
          isHref: true
        },
        {
          label: '院系管理',
          url: '/admin/departments/departments_list',
          isHref: true
        },
        {
          label: '教师管理',
          url: '/admin/teacher/teacher_list',
          isHref: true
        }
      ]
    },
    {
      label: '学期目标管理',
      url: '/admin/term/term_list',
      isHref: true,
    },
    {
      label: '课程班级管理',
      url: '/admin/classes/cla_list',
      isHref: true,
    },
    {
      label: '学生管理',
      url: '/admin/student/stu_list',
      isHref: true,
    },
    {
      label: '蓝牙设备管理',
      url: '/admin/beacon/bea_list',
      isHref: true,
    },
    {
      label: '文章列表',
      url: '/admin/article/art_list',
      isHref: true,
    },
    {
      label: '彩蛋活动列表',
      url: '/admin/activity/activity_list',
      isHref: true,
    },
    {
      label: '场地运动',
      url: '/movement/list',
    },
    {
      label: '轮播图管理',
      url: '/admin/carousel/car_list',
      isHref: true,
    },
    {
      label: '引导图管理',
      url: '/admin/guide/guide_list',
      isHref: true,
    },
    {
      label: 'app管理',
      url: '/admin/app/app_list',
      isHref: true,
    }
  ]
)
