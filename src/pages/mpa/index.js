// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import http from './http'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui'
import JadeTable from './components/JadeTable.vue'
import 'element-ui/lib/theme-chalk/index.css'
import './mock/mock'

var app = {
  /**
   * 加载Vue
   */
  loadVue() {

    /* 设置为 false 以阻止 vue 在启动时生成生产提示 */
    Vue.config.productionTip = false

    /* 用于开发环境 */
    Vue.config.devtools = true

    /* 开发中追踪组件初始化、编译、渲染和打补丁的性能 */
    Vue.config.performance = true

    /* 全局错误处理 */
    Vue.config.errorHandler = function(err, vm, info) {
      // handle error
      // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
      // TODO
      console.error(err)
    }
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      template: '<App/>',
      components: {App}
    })
  },
  /**
   * 注册插件
   */
  loadUse() {
    Vue.use(VueAxios, http)
    Vue.use(ElementUI)
  },
  /**
   * 注册全局组件
   */
  loadCustomComponent() {
    Vue.component('JadeTable', JadeTable)
  },
  /**
   * 初始化路由
   */
  initRouter() {
    window.localStorage.setItem('userId', 1)
    router.beforeEach((to, from, next) => {
      if (window.localStorage && window.localStorage.getItem('userId')) {
        console.log('to', to)
        console.log('userId:' + window.localStorage.getItem('userId'))
        next()
      } else {
        window.location.href = window.location.origin + '/admin/login'
      }
    })
  },
  /**
   * 初始化
   */
  init() {
    this.loadCustomComponent()
    this.loadUse()
    this.initRouter()
    this.loadVue()
  },
}
app.init()
