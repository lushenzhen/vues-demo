const fs = require('fs')
const path = require('path')
const config = require('../config')
let dirs = fs.readdirSync(path.resolve(__dirname, '../src/pages'))
console.log(dirs)

module.exports = {
  getEntries() {
    let obj = {}
    dirs.forEach(item => {
      obj[item] = './src/pages/' + item
    })
    return obj
  },
  getHtmlWebpackPlugins() {
    let arr = dirs.map(item => {
      return {
        filename: process.env.NODE_ENV == 'production' ? item + '.html' : (process.env.PAGE || config.dev.defaultPage) == item ? 'index.html' : item + '.html',
        template: './src/pages/' + item + '/index.html',
        chunks: ['manifest', 'vendor', item],
        inject: true,
      }
    })
    return arr;
  }
}
